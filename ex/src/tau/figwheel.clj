(ns tau.figwheel)

(defn handler [request]
  (if (and (= :get (:request-method request))
           (= "/"  (:uri request)))
    {:status 200
     :headers {"Content-Type" "text/html"
               "Cross-Origin-Opener-Policy" "same-origin"
               "Cross-Origin-Embedder-Policy" "require-corp"}
     :body (slurp "./ex/resources/public/figwheel_index.html")}
    {:status 404
     :headers {"Content-Type" "text/plain"}
     :body "Not Found"}))
