// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.call');
goog.require('cljs.core');
goog.require('clojure.string');
goog.require('tau.alpha.state');
goog.require('tau.alpha.util');
goog.require('tau.alpha.io');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.call.get_raw_fn = (function tau$alpha$call$get_raw_fn(y){
var s_QMARK_ = typeof y === 'string';
var raw_QMARK_ = ((s_QMARK_)?cljs.core._EQ_.call(null,"function",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(8),y))):cljs.core._EQ_.call(null,"function",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(8),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(y)].join('')))));
var x = cljs.core.apply.call(null,cljs.core.str,cljs.core.butlast.call(null,cljs.core.butlast.call(null,cljs.core.pr_str.call(null,y))));
var last_quote_QMARK_ = cljs.core._EQ_.call(null,"\"",cljs.core.last.call(null,x));
var x__$1 = ((last_quote_QMARK_)?cljs.core.apply.call(null,cljs.core.str,cljs.core.butlast.call(null,cljs.core.butlast.call(null,x))):x);
var x__$2 = cljs.core.apply.call(null,cljs.core.str,cljs.core.drop.call(null,(18),x__$1));
if(raw_QMARK_){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(y)].join('');
} else {
return cljs.core.apply.call(null,cljs.core.str,cljs.core.rest.call(null,cljs.core.drop_while.call(null,((function (s_QMARK_,raw_QMARK_,x,last_quote_QMARK_,x__$1,x__$2){
return (function (p1__9224_SHARP_){
return cljs.core.not.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["\"",null], null), null).call(null,p1__9224_SHARP_));
});})(s_QMARK_,raw_QMARK_,x,last_quote_QMARK_,x__$1,x__$2))
,x__$2)));
}
});
tau.alpha.call.obj_QMARK_ = (function tau$alpha$call$obj_QMARK_(x){
return cljs.core._EQ_.call(null,"#object[",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(8),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(x)].join(''))));
});
tau.alpha.call.strip_obj = (function tau$alpha$call$strip_obj(x){
var drop_front = cljs.core.rest.call(null,cljs.core.drop_while.call(null,(function (p1__9225_SHARP_){
return cljs.core.not.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["\"",null], null), null).call(null,p1__9225_SHARP_));
}),x));
var drop_back = cljs.core.apply.call(null,cljs.core.str,cljs.core.butlast.call(null,cljs.core.butlast.call(null,drop_front)));
if(cljs.core.truth_(tau.alpha.call.obj_QMARK_.call(null,x))){
return drop_back;
} else {
return x;
}
});
tau.alpha.call.get_name = (function tau$alpha$call$get_name(obj_js){
var s = ((typeof obj_js === 'string')?obj_js:cljs.core.pr_str.call(null,obj_js));
var new_s = tau.alpha.call.strip_obj.call(null,s);
var anon_QMARK_ = cljs.core._EQ_.call(null,"function (",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(10),new_s)));
var min_anon_QMARK_ = cljs.core._EQ_.call(null,"function(",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(9),new_s)));
var n1 = ((anon_QMARK_)?"Function":((min_anon_QMARK_)?"minFunction":cljs.core.apply.call(null,cljs.core.str,cljs.core.take_while.call(null,((function (s,new_s,anon_QMARK_,min_anon_QMARK_){
return (function (p1__9226_SHARP_){
return cljs.core.not.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["(",null], null), null).call(null,p1__9226_SHARP_));
});})(s,new_s,anon_QMARK_,min_anon_QMARK_))
,cljs.core.drop.call(null,(9),new_s)))));
return n1;
});
tau.alpha.call.name_fn_proto = (function tau$alpha$call$name_fn_proto(s){
var old_name = tau.alpha.call.get_name.call(null,s);
var f_QMARK_ = cljs.core._EQ_.call(null,"Function",old_name);
var minf_QMARK_ = cljs.core._EQ_.call(null,"minFunction",old_name);
var n = ["f_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.gensym.call(null))].join('');
if(f_QMARK_){
return ["function ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.subs.call(null,tau.alpha.call.get_raw_fn.call(null,s),(9))),";",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n)].join('');
} else {
if(minf_QMARK_){
return ["function ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n),cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.subs.call(null,tau.alpha.call.get_raw_fn.call(null,s),(8))),";",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n)].join('');
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.call.strip_obj.call(null,s)),";",cljs.core.str.cljs$core$IFn$_invoke$arity$1(old_name)].join('');
}
}
});
tau.alpha.call.name_fn = (function tau$alpha$call$name_fn(s){
var f = tau.alpha.call.name_fn_proto.call(null,s);
var es_QMARK_ = cljs.core.filter.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["\\",null], null), null),f);
if(!(cljs.core.empty_QMARK_.call(null,es_QMARK_))){
return tau.alpha.util.read.call(null,["\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(f),"\""].join(''));
} else {
return f;
}
});
tau.alpha.call.respile = (function tau$alpha$call$respile(fn_str){
return eval(tau.alpha.call.name_fn.call(null,fn_str));
});
tau.alpha.call.call = (function tau$alpha$call$call(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9229 = arguments.length;
var i__4532__auto___9230 = (0);
while(true){
if((i__4532__auto___9230 < len__4531__auto___9229)){
args__4534__auto__.push((arguments[i__4532__auto___9230]));

var G__9231 = (i__4532__auto___9230 + (1));
i__4532__auto___9230 = G__9231;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((1) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((1)),(0),null)):null);
return tau.alpha.call.call.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__4535__auto__);
});

tau.alpha.call.call.cljs$core$IFn$_invoke$arity$variadic = (function (afn,args){
if(cljs.core.truth_(afn)){
if(cljs.core.empty_QMARK_.call(null,args)){
return tau.alpha.call.respile.call(null,afn).call(null);
} else {
return cljs.core.apply.call(null,tau.alpha.call.respile.call(null,afn),args);
}
} else {
return tau.alpha.io.plog.call(null,"tau.alpha.call",74,null,"call - afn undefined:",afn);
}
});

tau.alpha.call.call.cljs$lang$maxFixedArity = (1);

/** @this {Function} */
tau.alpha.call.call.cljs$lang$applyTo = (function (seq9227){
var G__9228 = cljs.core.first.call(null,seq9227);
var seq9227__$1 = cljs.core.next.call(null,seq9227);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9228,seq9227__$1);
});

tau.alpha.call.str_fn_QMARK_ = (function tau$alpha$call$str_fn_QMARK_(s){
return ((typeof s === 'string') && (cljs.core._EQ_.call(null,"function",cljs.core.apply.call(null,cljs.core.str,cljs.core.take.call(null,(8),cljs.core.apply.call(null,cljs.core.str,cljs.core.rest.call(null,cljs.core.drop_while.call(null,(function (p1__9232_SHARP_){
return cljs.core.not.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["\"",null], null), null).call(null,p1__9232_SHARP_));
}),s))))))));
});
tau.alpha.call.respile_args = (function tau$alpha$call$respile_args(args){
return cljs.core.map.call(null,(function (p1__9233_SHARP_){
if(cljs.core.truth_(tau.alpha.call.str_fn_QMARK_.call(null,p1__9233_SHARP_))){
return tau.alpha.call.respile.call(null,p1__9233_SHARP_);
} else {
return p1__9233_SHARP_;
}
}),args);
});
tau.alpha.call.call_handler = (function tau$alpha$call$call_handler(p__9234){
var map__9235 = p__9234;
var map__9235__$1 = ((((!((map__9235 == null)))?(((((map__9235.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9235.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9235):map__9235);
var afn = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"afn","afn",-1423568060));
var ab = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"ab","ab",-839573926));
var args = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var from = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var to = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"to","to",192099007));
var opts = cljs.core.get.call(null,map__9235__$1,new cljs.core.Keyword(null,"opts","opts",155075701));
if(cljs.core.truth_(tau.alpha.util.typed_array_QMARK_.call(null,ab))){
return cljs.core.apply.call(null,tau.alpha.call.respile.call(null,afn),cljs.core.cons.call(null,ab,tau.alpha.call.respile_args.call(null,args)));
} else {
try{return cljs.core.apply.call(null,tau.alpha.call.respile.call(null,afn),tau.alpha.call.respile_args.call(null,args));
}catch (e9237){var e = e9237;
if(cljs.core.truth_(new cljs.core.Keyword(null,"error-fn","error-fn",-171437615).cljs$core$IFn$_invoke$arity$1(opts))){
var error_fn = tau.alpha.util.read.call(null,cljs.core.pr_str.call(null,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615).cljs$core$IFn$_invoke$arity$1(opts)));
if(!(cljs.core._EQ_.call(null,"nil",error_fn))){
return tau.alpha.call.respile.call(null,error_fn).call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"from","from",1815293044),from,new cljs.core.Keyword(null,"to","to",192099007),to,new cljs.core.Keyword(null,"afn","afn",-1423568060),afn,new cljs.core.Keyword(null,"args","args",1315556576),args,new cljs.core.Keyword(null,"error","error",-978969032),e], null));
} else {
return null;
}
} else {
return tau.alpha.io.plog.call(null,"tau.alpha.call",100,null,"ERROR:",e,"- no :error-fn defined.\n afn:",afn,"\n args:",args,"\nfrom:",from,"\nto:",to,"\nopts:",opts);
}
}}
});
tau.alpha.state.add_handler.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),tau.alpha.call.call_handler], null));

//# sourceMappingURL=call.js.map
