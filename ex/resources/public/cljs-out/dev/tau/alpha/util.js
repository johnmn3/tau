// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.util');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('clojure.string');
goog.require('tau.alpha.state');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"object","object",-1179821820,null),(function (p1__8055_SHARP_){
return ["#object",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8055_SHARP_)].join('');
}));
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"error","error",661562495,null),(function (p1__8056_SHARP_){
return ["#error",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8056_SHARP_)].join('');
}));
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"Error","Error",-1692662047,null),(function (p1__8057_SHARP_){
return ["#error",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8057_SHARP_)].join('');
}));
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"ExceptionInfo","ExceptionInfo",294935087,null),(function (p1__8058_SHARP_){
return ["#exception-info",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8058_SHARP_)].join('');
}));
cljs.reader.register_tag_parser_BANG_.call(null,new cljs.core.Symbol(null,"Nothing","Nothing",1764448907,null),(function (p1__8059_SHARP_){
return ["#nothing",cljs.core.str.cljs$core$IFn$_invoke$arity$1(p1__8059_SHARP_)].join('');
}));
tau.alpha.util.deserialize = (function tau$alpha$util$deserialize(data){
return cljs.reader.read_string.call(null,data);
});
tau.alpha.util.serialize = (function tau$alpha$util$serialize(data){
return cljs.core.pr_str.call(null,data);
});
tau.alpha.util.read = (function tau$alpha$util$read(data){
return cljs.reader.read_string.call(null,data);
});
tau.alpha.util.write = (function tau$alpha$util$write(data){
return cljs.core.pr_str.call(null,data);
});
tau.alpha.util.pr_fn = (function tau$alpha$util$pr_fn(afn){
return cljs.core.pr_str.call(null,cljs.core.pr_str.call(null,afn));
});
tau.alpha.util.time_since = (function tau$alpha$util$time_since(x){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(((new Date()).getTime() - x))," milliseconds"].join('');
});
/**
 * Tests whether a given `value` is a typed array.
 */
tau.alpha.util.typed_array_QMARK_ = (function tau$alpha$util$typed_array_QMARK_(value){
var value_type = cljs.core.type.call(null,value);
return ((cljs.core._EQ_.call(null,value_type,ArrayBuffer)) || (cljs.core._EQ_.call(null,value_type,SharedArrayBuffer)) || (cljs.core._EQ_.call(null,value_type,Int8Array)) || (cljs.core._EQ_.call(null,value_type,Uint8Array)) || (cljs.core._EQ_.call(null,value_type,Uint8ClampedArray)) || (cljs.core._EQ_.call(null,value_type,Int16Array)) || (cljs.core._EQ_.call(null,value_type,Uint16Array)) || (cljs.core._EQ_.call(null,value_type,Int32Array)) || (cljs.core._EQ_.call(null,value_type,Uint32Array)) || (cljs.core._EQ_.call(null,value_type,Float32Array)) || (cljs.core._EQ_.call(null,value_type,Float64Array)));
});
tau.alpha.util.on_QMARK_ = (function tau$alpha$util$on_QMARK_(fname){
return cljs.core._EQ_.call(null,fname,tau.alpha.state.id);
});
tau.alpha.util.if_ojb__GT_str = (function tau$alpha$util$if_ojb__GT_str(o){
if(typeof o === 'string'){
return o;
} else {
return cljs.core.pr_str.call(null,o);
}
});
tau.alpha.util.if_str__GT_obj = (function tau$alpha$util$if_str__GT_obj(s){
if(typeof s === 'string'){
return cljs.reader.read_string.call(null,s);
} else {
if(cljs.core.array_QMARK_.call(null,s)){
return cljs.reader.read_string.call(null,s);
} else {
return s;
}
}
});
tau.alpha.util.id_counter = cljs.core.atom.call(null,(0));
tau.alpha.util.new_id = (function tau$alpha$util$new_id(){
var v = cljs.core.deref.call(null,tau.alpha.util.id_counter);
var r = (((v < (10)))?["00",cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)].join(''):null);
var r__$1 = (function (){var or__3949__auto__ = r;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
if((v < (100))){
return ["0",cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)].join('');
} else {
return null;
}
}
})();
var r__$2 = (function (){var or__3949__auto__ = r__$1;
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)].join('');
}
})();
cljs.core.swap_BANG_.call(null,tau.alpha.util.id_counter,cljs.core.inc);

return r__$2;
});
tau.alpha.util.scripts_src = (function tau$alpha$util$scripts_src(){
var scripts = document.getElementsByTagName("SCRIPT");
return cljs.core.map.call(null,((function (scripts){
return (function (p1__8061_SHARP_){
return p1__8061_SHARP_.src;
});})(scripts))
,cljs.core.remove.call(null,((function (scripts){
return (function (p1__8060_SHARP_){
return cljs.core.empty_QMARK_.call(null,p1__8060_SHARP_.src);
});})(scripts))
,(function (){var iter__4324__auto__ = ((function (scripts){
return (function tau$alpha$util$scripts_src_$_iter__8062(s__8063){
return (new cljs.core.LazySeq(null,((function (scripts){
return (function (){
var s__8063__$1 = s__8063;
while(true){
var temp__5457__auto__ = cljs.core.seq.call(null,s__8063__$1);
if(temp__5457__auto__){
var s__8063__$2 = temp__5457__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,s__8063__$2)){
var c__4322__auto__ = cljs.core.chunk_first.call(null,s__8063__$2);
var size__4323__auto__ = cljs.core.count.call(null,c__4322__auto__);
var b__8065 = cljs.core.chunk_buffer.call(null,size__4323__auto__);
if((function (){var i__8064 = (0);
while(true){
if((i__8064 < size__4323__auto__)){
var i = cljs.core._nth.call(null,c__4322__auto__,i__8064);
cljs.core.chunk_append.call(null,b__8065,(scripts[i]));

var G__8066 = (i__8064 + (1));
i__8064 = G__8066;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8065),tau$alpha$util$scripts_src_$_iter__8062.call(null,cljs.core.chunk_rest.call(null,s__8063__$2)));
} else {
return cljs.core.chunk_cons.call(null,cljs.core.chunk.call(null,b__8065),null);
}
} else {
var i = cljs.core.first.call(null,s__8063__$2);
return cljs.core.cons.call(null,(scripts[i]),tau$alpha$util$scripts_src_$_iter__8062.call(null,cljs.core.rest.call(null,s__8063__$2)));
}
} else {
return null;
}
break;
}
});})(scripts))
,null,null));
});})(scripts))
;
return iter__4324__auto__.call(null,cljs.core.range.call(null,scripts.length));
})()));
});
tau.alpha.util.create_worker_body = (function tau$alpha$util$create_worker_body(require_ns,afn,value,tid){
var multi_loader = ["var taupreload = {};\n","taupreload.fn = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(afn),"`;\n","taupreload.args = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"`;\n","taupreload.tid = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid),"`;\n","CLOSURE_BASE_PATH = '",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),"';\n","this.CLOSURE_IMPORT_SCRIPT = (function(global) {\n","    return function(src) {\n","        global['importScripts'](src);\n","        return true;\n","    };\n","})(this);\n","if(typeof goog == 'undefined') importScripts(CLOSURE_BASE_PATH + 'base.js');\n","importScripts('",cljs.core.str.cljs$core$IFn$_invoke$arity$1([cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.basePath),"../cljs_deps.js"].join('')),"');\n","goog.require('",cljs.core.str.cljs$core$IFn$_invoke$arity$1(require_ns),"');\n"].join('');
var single_loader = ["var taupreload = {};\n","taupreload.fn = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(afn),"`;\n","taupreload.args = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"`;\n","taupreload.tid = `",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tid),"`;\n","importScripts('",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.last.call(null,tau.alpha.util.scripts_src.call(null))),"');\n"].join('');
if(cljs.core.empty_QMARK_.call(null,goog.basePath)){
return single_loader;
} else {
return multi_loader;
}
});
tau.alpha.util.mk_body = (function tau$alpha$util$mk_body(nsname,afn,value,tid){
return tau.alpha.util.create_worker_body.call(null,nsname,afn,value,tid);
});
tau.alpha.util.get_worker_script = (function tau$alpha$util$get_worker_script(fname,nsname,afn,value,tid){
return URL.createObjectURL((new Blob(cljs.core.clj__GT_js.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [tau.alpha.util.mk_body.call(null,nsname,afn,cljs.core.pr_str.call(null,value),tid)], null)))));
});
tau.alpha.util.htb64 = function hexToBase64(hexstring) {
    return btoa(hexstring.match(/\w{2}/g).map(function(a) {
        return String.fromCharCode(parseInt(a, 16));
    }).join(""));
};
tau.alpha.util.new_b64_uuid = (function tau$alpha$util$new_b64_uuid(){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.butlast.call(null,cljs.core.butlast.call(null,tau.alpha.util.htb64.call(null,cljs.core.apply.call(null,cljs.core.str,cljs.core.filter.call(null,(function (p1__8067_SHARP_){
return cljs.core.not.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, ["-",null], null), null).call(null,p1__8067_SHARP_));
}),[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.random_uuid.call(null))].join('')))))));
});
tau.alpha.util.strip_onium_tag = (function tau$alpha$util$strip_onium_tag(s){
if(cljs.core.truth_(s.startsWith("#Onium"))){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.drop_last.call(null,cljs.core.drop.call(null,(13),s)));
} else {
if(cljs.core.truth_(s.startsWith("#Neutrino"))){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.drop_last.call(null,cljs.core.drop.call(null,(16),s)));
} else {
if(cljs.core.truth_([cljs.core.str.cljs$core$IFn$_invoke$arity$1(s)].join('').startsWith(":tau.alpha.on/t-"))){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.drop_last.call(null,cljs.core.drop.call(null,(10),s)));
} else {
if(cljs.core.truth_([cljs.core.str.cljs$core$IFn$_invoke$arity$1(s)].join('').startsWith(":tau/on"))){
return cljs.core.apply.call(null,cljs.core.str,cljs.core.drop_last.call(null,cljs.core.drop.call(null,(10),s)));
} else {
return s;
}
}
}
}
});
tau.alpha.util.get_new_id = (function tau$alpha$util$get_new_id(){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(clojure.string.replace.call(null,(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))?["t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.util.new_b64_uuid.call(null))].join(''):[cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.util.strip_onium_tag.call(null,tau.alpha.state.id)),"_",cljs.core.str.cljs$core$IFn$_invoke$arity$1(tau.alpha.util.new_b64_uuid.call(null))].join('')),/\//,"-"))].join('');
});
tau.alpha.util.padnum = (function tau$alpha$util$padnum(n){
var s = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(n)].join('');
var l = ((6) - cljs.core.count.call(null,s));
var p = cljs.core.take.call(null,l,cljs.core.cycle.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [","], null)));
var ps = cljs.core.apply.call(null,cljs.core.str,cljs.core.concat.call(null,p,s));
return ps;
});
tau.alpha.util.enc = (function tau$alpha$util$enc(ar,d){
try{var s = cljs.core.pr_str.call(null,d);
var new_length = cljs.core.count.call(null,s);
var _ = (((((2) * new_length) < ar.length))?null:(function(){throw (new Error("Assert failed: (< (* 2 new-length) (.-length ar))"))})());
var old_index = Atomics.load(ar,(2));
var new_index = ((cljs.core._EQ_.call(null,(0),old_index))?((ar.length / (2)) | (0)):(0));
var write_point = ((cljs.core._EQ_.call(null,(0),new_index))?(4):(1));
var ___$1 = cljs.core.doall.call(null,cljs.core.map_indexed.call(null,((function (s,new_length,_,old_index,new_index,write_point){
return (function (p1__8068_SHARP_,p2__8069_SHARP_){
return (ar[((p1__8068_SHARP_ + write_point) + new_index)] = p2__8069_SHARP_.charCodeAt((0)));
});})(s,new_length,_,old_index,new_index,write_point))
,cljs.core.seq.call(null,s)));
Atomics.store(ar,((write_point - (1)) + new_index),new_length);

Atomics.store(ar,(2),new_index);

return ar;
}catch (e8070){var e = e8070;
return cljs.core.println.call(null,"failed on enc");
}});
tau.alpha.util.unc = (function tau$alpha$util$unc(a){
var i = Atomics.load(a,(2));
var read_point = ((cljs.core._EQ_.call(null,(0),i))?(4):(1));
var l = Atomics.load(a,((read_point - (1)) + i));
var s = cljs.core.apply.call(null,cljs.core.str,cljs.core.map.call(null,((function (i,read_point,l){
return (function (p1__8071_SHARP_){
return String.fromCharCode(p1__8071_SHARP_);
});})(i,read_point,l))
,cljs.core.take.call(null,l,cljs.core.drop.call(null,(i + read_point),cljs.core.array_seq.call(null,a)))));
return tau.alpha.util.read.call(null,s);
});
tau.alpha.util.sleep = (function tau$alpha$util$sleep(msec){
var deadline = (msec + (new Date()).getTime());
while(true){
if((deadline > (new Date()).getTime())){
continue;
} else {
return null;
}
break;
}
});

//# sourceMappingURL=util.js.map
