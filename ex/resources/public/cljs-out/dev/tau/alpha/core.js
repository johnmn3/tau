// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.core');
goog.require('cljs.core');
goog.require('tau.alpha.state');
goog.require('tau.alpha.on');
goog.require('tau.alpha.tau');
tau.alpha.core.set_conf_BANG_ = tau.alpha.state.set_conf_BANG_;
tau.alpha.core.on_screen_QMARK_ = tau.alpha.state.on_screen_QMARK_;
tau.alpha.core.tauon = tau.alpha.on.tauon;
tau.alpha.core.tau = tau.alpha.tau.tau;
tau.alpha.core.db = tau.alpha.tau.db;
tau.alpha.core.reconstruct_tau = tau.alpha.tau.reconstruct_tau;
tau.alpha.core.get_id = tau.alpha.tau._id;

//# sourceMappingURL=core.js.map
