// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.ex');
goog.require('cljs.core');
goog.require('figwheel.repl');
goog.require('tau.alpha.state');
goog.require('tau.alpha.on');
goog.require('tau.alpha.exec');
goog.require('tau.alpha.future');
goog.require('tau.alpha.util');
goog.require('tau.alpha.tau');
cljs.core.enable_console_print_BANG_.call(null);
tau.alpha.state.set_conf_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"main","main",-2117802661),"tau.alpha.ex",new cljs.core.Keyword(null,"log?","log?",-366002723),true], null));
tau.alpha.ex.handle_response = (function tau$alpha$ex$handle_response(event){
var xhr = event.target;
if(cljs.core._EQ_.call(null,xhr.readyState,(4))){
if(cljs.core._EQ_.call(null,xhr.status,(200))){
var res = xhr.responseText;
if(cljs.core.truth_(res)){
return tau.alpha.future.yield$.call(null,res);
} else {
return null;
}
} else {
return tau.alpha.future.yield$.call(null,xhr.statusText);
}
} else {
return null;
}
});
tau.alpha.ex.xget = (function tau$alpha$ex$xget(url){
return tau.alpha.future.future_call.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [url], null),(function (url__$1){
var axhr = (new XMLHttpRequest());
axhr.open("GET",url__$1);

axhr.onreadystatechange = tau.alpha.ex.handle_response;

return axhr.send(null);
}));
});
tau.alpha.ex.x = "x";
tau.alpha.ex.y = (9);
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
tau.alpha.ex.z = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"one","one",935007904),(1),new cljs.core.Keyword(null,"two","two",627606869),(2)], null);
} else {
}

//# sourceMappingURL=ex.js.map
