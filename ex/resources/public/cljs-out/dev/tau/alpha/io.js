// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.io');
goog.require('cljs.core');
goog.require('cljs.reader');
goog.require('tau.alpha.state');
goog.require('tau.alpha.util');
goog.require('cljs.pprint');
cljs.core.enable_console_print_BANG_.call(null);
cljs.core._STAR_print_fn_bodies_STAR_ = true;
tau.alpha.io.do_print = (function tau$alpha$io$do_print(s){
return cljs.pprint.pprint.call(null,s);
});
tau.alpha.state.add_handler.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"do-print","do-print",1112838602),tau.alpha.io.do_print], null));
tau.alpha.io.post = (function tau$alpha$io$post(worker,t,copying_data,transferable_keys){
var without_transferables = cljs.core.reduce.call(null,(function (p1__9187_SHARP_,p2__9188_SHARP_){
return cljs.core.assoc_in.call(null,p1__9187_SHARP_,p2__9188_SHARP_,null);
}),copying_data,transferable_keys);
var transferables = cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.call(null,((function (without_transferables){
return (function (key){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tau.alpha.util.serialize.call(null,key),cljs.core.get_in.call(null,copying_data,key)], null);
});})(without_transferables))
,transferable_keys));
var serialized = tau.alpha.util.serialize.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),t,new cljs.core.Keyword(null,"data","data",-232669377),without_transferables], null));
var copying = cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 2, ["serialized",serialized,"transferables",transferables], null));
return worker.postMessage(copying,((cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"port","port",1534937262)], null)], null),transferable_keys))?cljs.core.clj__GT_js.call(null,(function (){var or__3949__auto__ = cljs.core.vals.call(null,transferables);
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
})()):[]));
});
tau.alpha.io.worker = (function tau$alpha$io$worker(script,handlers){
var w = (new Worker(script));
w.addEventListener(tau.alpha.state.event_message,cljs.core.partial.call(null,tau.alpha.state.message_handler,handlers));

return w;
});
tau.alpha.io.screen_GT_ = (function tau$alpha$io$screen_GT_(var_args){
var G__9190 = arguments.length;
switch (G__9190) {
case 4:
return tau.alpha.io.screen_GT_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 3:
return tau.alpha.io.screen_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.io.screen_GT_.cljs$core$IFn$_invoke$arity$4 = (function (b,tag,copying_data,transferable_keys){
return tau.alpha.io.post.call(null,b,tag,copying_data,transferable_keys);
});

tau.alpha.io.screen_GT_.cljs$core$IFn$_invoke$arity$3 = (function (b,tag,copying_data){
if(cljs.core.truth_(new cljs.core.Keyword(null,"array-buffer","array-buffer",519008380).cljs$core$IFn$_invoke$arity$1(copying_data))){
return tau.alpha.io.screen_GT_.call(null,b,tag,copying_data,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"array-buffer","array-buffer",519008380)], null)], null));
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089).cljs$core$IFn$_invoke$arity$1(copying_data))){
return tau.alpha.io.screen_GT_.call(null,b,tag,copying_data,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ab","ab",-839573926)], null)], null));
} else {
return tau.alpha.io.screen_GT_.call(null,b,tag,copying_data,cljs.core.PersistentVector.EMPTY);
}
}
});

tau.alpha.io.screen_GT_.cljs$lang$maxFixedArity = 4;

tau.alpha.io.screen_LT_ = (function tau$alpha$io$screen_LT_(var_args){
var G__9193 = arguments.length;
switch (G__9193) {
case 3:
return tau.alpha.io.screen_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 2:
return tau.alpha.io.screen_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.io.screen_LT_.cljs$core$IFn$_invoke$arity$3 = (function (tag,copying_data,transferable_keys){
return tau.alpha.io.post.call(null,self,tag,copying_data,transferable_keys);
});

tau.alpha.io.screen_LT_.cljs$core$IFn$_invoke$arity$2 = (function (tag,copying_data){
if(cljs.core.truth_(new cljs.core.Keyword(null,"array-buffer","array-buffer",519008380).cljs$core$IFn$_invoke$arity$1(copying_data))){
return tau.alpha.io.screen_LT_.call(null,tag,copying_data,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"array-buffer","array-buffer",519008380)], null)], null));
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089).cljs$core$IFn$_invoke$arity$1(copying_data))){
return tau.alpha.io.screen_LT_.call(null,tag,copying_data,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ab","ab",-839573926)], null)], null));
} else {
return tau.alpha.io.screen_LT_.call(null,tag,copying_data,cljs.core.PersistentVector.EMPTY);
}
}
});

tau.alpha.io.screen_LT_.cljs$lang$maxFixedArity = 3;

tau.alpha.io.prepare = (function tau$alpha$io$prepare(copying_data,transferable_keys){
var without_transferables = cljs.core.reduce.call(null,(function (p1__9195_SHARP_,p2__9196_SHARP_){
return cljs.core.assoc_in.call(null,p1__9195_SHARP_,p2__9196_SHARP_,null);
}),copying_data,transferable_keys);
var transferables = cljs.core.into.call(null,cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.call(null,((function (without_transferables){
return (function (key){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tau.alpha.util.serialize.call(null,key),cljs.core.get_in.call(null,copying_data,key)], null);
});})(without_transferables))
,transferable_keys));
var serialized = tau.alpha.util.serialize.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"data","data",-232669377),without_transferables], null));
var copying = cljs.core.clj__GT_js.call(null,new cljs.core.PersistentArrayMap(null, 2, ["serialized",serialized,"transferables",transferables], null));
return copying;
});
tau.alpha.io.transmit = (function tau$alpha$io$transmit(tid,m){
var p = new cljs.core.Keyword(null,"port","port",1534937262).cljs$core$IFn$_invoke$arity$1(cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.state.ports),tid));
if(cljs.core.truth_(p)){
return p.postMessage((cljs.core.truth_(new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089).cljs$core$IFn$_invoke$arity$1(m))?tau.alpha.io.prepare.call(null,m,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ab","ab",-839573926)], null)], null)):tau.alpha.io.prepare.call(null,m,cljs.core.PersistentVector.EMPTY)),[]);
} else {
return null;
}
});
tau.alpha.io.fix_arg = (function tau$alpha$io$fix_arg(arg){
if(cljs.core.fn_QMARK_.call(null,arg)){
return cljs.core.pr_str.call(null,arg);
} else {
return arg;
}
});
tau.alpha.io.fix_args = (function tau$alpha$io$fix_args(args){
return cljs.core.map.call(null,tau.alpha.io.fix_arg,args);
});
tau.alpha.io.do_on = (function tau$alpha$io$do_on(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9201 = arguments.length;
var i__4532__auto___9202 = (0);
while(true){
if((i__4532__auto___9202 < len__4531__auto___9201)){
args__4534__auto__.push((arguments[i__4532__auto___9202]));

var G__9203 = (i__4532__auto___9202 + (1));
i__4532__auto___9202 = G__9203;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((3) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((3)),(0),null)):null);
return tau.alpha.io.do_on.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4535__auto__);
});

tau.alpha.io.do_on.cljs$core$IFn$_invoke$arity$variadic = (function (obj,afn,opts,args){
var t = (((obj instanceof cljs.core.Keyword))?obj:[cljs.core.str.cljs$core$IFn$_invoke$arity$1(obj)].join(''));
if(cljs.core._EQ_.call(null,tau.alpha.state.get_id.call(null),t)){
return cljs.core.apply.call(null,afn,args);
} else {
if(((cljs.core._EQ_.call(null,"screen",t)) || (cljs.core._EQ_.call(null,cljs.core.symbol.call(null,"screen"),t)))){
return tau.alpha.io.screen_LT_.call(null,new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),(cljs.core.truth_(tau.alpha.util.typed_array_QMARK_.call(null,cljs.core.first.call(null,args)))?new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"args","args",1315556576),((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,args)))?cljs.core.List.EMPTY:cljs.core.rest.call(null,args)),new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),opts,new cljs.core.Keyword(null,"ab","ab",-839573926),cljs.core.first.call(null,args),new cljs.core.Keyword(null,"afn","afn",-1423568060),cljs.core.pr_str.call(null,afn),new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089),true], null):new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"args","args",1315556576),tau.alpha.io.fix_args.call(null,args),new cljs.core.Keyword(null,"afn","afn",-1423568060),tau.alpha.util.write.call(null,afn),new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.update.call(null,opts,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),tau.alpha.io.fix_arg)], null)));
} else {
if(cljs.core._EQ_.call(null,"screen",tau.alpha.state.get_id.call(null))){
return tau.alpha.io.screen_GT_.call(null,new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.state.ports),t)),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),(cljs.core.truth_(tau.alpha.util.typed_array_QMARK_.call(null,cljs.core.first.call(null,args)))?new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"args","args",1315556576),((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,args)))?cljs.core.List.EMPTY:cljs.core.rest.call(null,args)),new cljs.core.Keyword(null,"afn","afn",-1423568060),tau.alpha.util.write.call(null,afn),new cljs.core.Keyword(null,"ab","ab",-839573926),cljs.core.first.call(null,args),new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089),true,new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.update.call(null,opts,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.pr_str)], null):new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"args","args",1315556576),tau.alpha.io.fix_args.call(null,args),new cljs.core.Keyword(null,"afn","afn",-1423568060),tau.alpha.util.write.call(null,afn),new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.update.call(null,opts,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.pr_str)], null)));
} else {
return tau.alpha.io.transmit.call(null,t,(cljs.core.truth_(tau.alpha.util.typed_array_QMARK_.call(null,cljs.core.first.call(null,args)))?new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"afn","afn",-1423568060),tau.alpha.util.write.call(null,afn),new cljs.core.Keyword(null,"args","args",1315556576),((cljs.core._EQ_.call(null,(1),cljs.core.count.call(null,args)))?cljs.core.List.EMPTY:cljs.core.rest.call(null,args)),new cljs.core.Keyword(null,"ab","ab",-839573926),cljs.core.first.call(null,args),new cljs.core.Keyword(null,"args-typed-array","args-typed-array",1303200089),true,new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.update.call(null,opts,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.pr_str)], null):new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"typed-call-here","typed-call-here",831213063),new cljs.core.Keyword(null,"afn","afn",-1423568060),tau.alpha.util.write.call(null,afn),new cljs.core.Keyword(null,"args","args",1315556576),tau.alpha.io.fix_args.call(null,args),new cljs.core.Keyword(null,"to","to",192099007),t,new cljs.core.Keyword(null,"from","from",1815293044),tau.alpha.state.id,new cljs.core.Keyword(null,"opts","opts",155075701),cljs.core.update.call(null,opts,new cljs.core.Keyword(null,"error-fn","error-fn",-171437615),cljs.core.pr_str)], null)));
}
}
}
});

tau.alpha.io.do_on.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
tau.alpha.io.do_on.cljs$lang$applyTo = (function (seq9197){
var G__9198 = cljs.core.first.call(null,seq9197);
var seq9197__$1 = cljs.core.next.call(null,seq9197);
var G__9199 = cljs.core.first.call(null,seq9197__$1);
var seq9197__$2 = cljs.core.next.call(null,seq9197__$1);
var G__9200 = cljs.core.first.call(null,seq9197__$2);
var seq9197__$3 = cljs.core.next.call(null,seq9197__$2);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9198,G__9199,G__9200,seq9197__$3);
});

tau.alpha.io.plog = (function tau$alpha$io$plog(var_args){
var args__4534__auto__ = [];
var len__4531__auto___9208 = arguments.length;
var i__4532__auto___9209 = (0);
while(true){
if((i__4532__auto___9209 < len__4531__auto___9208)){
args__4534__auto__.push((arguments[i__4532__auto___9209]));

var G__9210 = (i__4532__auto___9209 + (1));
i__4532__auto___9209 = G__9210;
continue;
} else {
}
break;
}

var argseq__4535__auto__ = ((((3) < args__4534__auto__.length))?(new cljs.core.IndexedSeq(args__4534__auto__.slice((3)),(0),null)):null);
return tau.alpha.io.plog.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4535__auto__);
});

tau.alpha.io.plog.cljs$core$IFn$_invoke$arity$variadic = (function (f,l,ret,s){
if(cljs.core.truth_((function (){var or__3949__auto__ = cljs.core.deref.call(null,tau.alpha.state.loaded_QMARK_);
if(cljs.core.truth_(or__3949__auto__)){
return or__3949__auto__;
} else {
return tau.alpha.state.on_screen_QMARK_.call(null);
}
})())){
if(cljs.core.truth_(new cljs.core.Keyword(null,"log?","log?",-366002723).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.conf)))){
if(cljs.core.not.call(null,tau.alpha.util.on_QMARK_.call(null,tau.alpha.state.screen))){
tau.alpha.io.screen_LT_.call(null,new cljs.core.Keyword(null,"do-print","do-print",1112838602),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"on","on",173873944),tau.alpha.state.get_id.call(null),new cljs.core.Keyword(null,"file","file",-1269645878),f,new cljs.core.Keyword(null,"line","line",212345235),l,new cljs.core.Keyword(null,"return-value","return-value",661899451),ret,new cljs.core.Keyword(null,"out","out",-910545517),s], null));
} else {
cljs.pprint.pprint.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"on","on",173873944),tau.alpha.state.get_id.call(null),new cljs.core.Keyword(null,"file","file",-1269645878),f,new cljs.core.Keyword(null,"line","line",212345235),l,new cljs.core.Keyword(null,"return-value","return-value",661899451),ret,new cljs.core.Keyword(null,"out","out",-910545517),s], null));
}
} else {
}

return ret;
} else {
return null;
}
});

tau.alpha.io.plog.cljs$lang$maxFixedArity = (3);

/** @this {Function} */
tau.alpha.io.plog.cljs$lang$applyTo = (function (seq9204){
var G__9205 = cljs.core.first.call(null,seq9204);
var seq9204__$1 = cljs.core.next.call(null,seq9204);
var G__9206 = cljs.core.first.call(null,seq9204__$1);
var seq9204__$2 = cljs.core.next.call(null,seq9204__$1);
var G__9207 = cljs.core.first.call(null,seq9204__$2);
var seq9204__$3 = cljs.core.next.call(null,seq9204__$2);
var self__4518__auto__ = this;
return self__4518__auto__.cljs$core$IFn$_invoke$arity$variadic(G__9205,G__9206,G__9207,seq9204__$3);
});

tau.alpha.io.receive_dist_port = (function tau$alpha$io$receive_dist_port(m){
cljs.core.swap_BANG_.call(null,tau.alpha.state.ports,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"tid","tid",-901350880).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"port","port",1534937262)], null),new cljs.core.Keyword(null,"port","port",1534937262).cljs$core$IFn$_invoke$arity$1(m));

cljs.core.doall.call(null,cljs.core.map.call(null,(function (p1__9211_SHARP_){
return p1__9211_SHARP_.call(null,new cljs.core.Keyword(null,"tid","tid",-901350880).cljs$core$IFn$_invoke$arity$1(m));
}),cljs.core.deref.call(null,tau.alpha.state.new_port_fns)));

return new cljs.core.Keyword(null,"port","port",1534937262).cljs$core$IFn$_invoke$arity$1(m).onmessage = (function (p1__9212_SHARP_){
return cljs.core.partial.call(null,tau.alpha.state.message_handler,cljs.core.deref.call(null,tau.alpha.state.serve_handlers)).call(null,p1__9212_SHARP_);
});
});
tau.alpha.state.add_handler.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"dist-port","dist-port",605116572),tau.alpha.io.receive_dist_port], null));
tau.alpha.io.kill_remote = (function tau$alpha$io$kill_remote(tid){
if(cljs.core.not.call(null,tau.alpha.state.on_screen_QMARK_.call(null))){
cljs.core.swap_BANG_.call(null,tau.alpha.state.ports,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tid,new cljs.core.Keyword(null,"port","port",1534937262)], null),(function (p1__9213_SHARP_){
return p1__9213_SHARP_.close();
}));
} else {
}

return cljs.core.swap_BANG_.call(null,tau.alpha.state.ports,cljs.core.dissoc,tid);
});
tau.alpha.io.kill_local = (function tau$alpha$io$kill_local(){
var remote_ports = cljs.core.keys.call(null,cljs.core.deref.call(null,tau.alpha.state.ports));
cljs.core.doall.call(null,cljs.core.map.call(null,((function (remote_ports){
return (function (p1__9214_SHARP_){
return tau.alpha.io.do_on.call(null,p1__9214_SHARP_,((function (remote_ports){
return (function (id){
return tau.alpha.io.kill_remote.call(null,id);
});})(remote_ports))
,null,tau.alpha.state.id);
});})(remote_ports))
,remote_ports));

tau.alpha.io.do_on.call(null,tau.alpha.state.screen,((function (remote_ports){
return (function (id){
return tau.alpha.io.kill_remote.call(null,id);
});})(remote_ports))
,null,tau.alpha.state.id);

return close();
});
tau.alpha.io.connect = (function tau$alpha$io$connect(var_args){
var G__9216 = arguments.length;
switch (G__9216) {
case 0:
return tau.alpha.io.connect.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return tau.alpha.io.connect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

tau.alpha.io.connect.cljs$core$IFn$_invoke$arity$0 = (function (){
if(cljs.core.truth_(tau.alpha.state.off_screen_QMARK_.call(null))){
var p = (function (){var pred__9217 = cljs.core._EQ_;
var expr__9218 = tau.alpha.state.get_id.call(null);
if(cljs.core.truth_(pred__9217.call(null,"hf",expr__9218))){
return (65);
} else {
return "not approved";
}
})();
if(cljs.core.not.call(null,new cljs.core.Keyword(null,"repl-fn","repl-fn",1511133534).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.conf)))){
return tau.alpha.io.plog.call(null,"tau.alpha.io",148,null,"no repl-fn set for this filo");
} else {
if(cljs.core.int_QMARK_.call(null,p)){
return new cljs.core.Keyword(null,"repl-fn","repl-fn",1511133534).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.conf)).call(null,p);
} else {
return null;
}
}
} else {
return null;
}
});

tau.alpha.io.connect.cljs$core$IFn$_invoke$arity$1 = (function (p){
return new cljs.core.Keyword(null,"repl-fn","repl-fn",1511133534).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.state.conf)).call(null,p);
});

tau.alpha.io.connect.cljs$lang$maxFixedArity = 1;

tau.alpha.io.send_port = (function tau$alpha$io$send_port(w,f,p){
return tau.alpha.io.screen_GT_.call(null,w,new cljs.core.Keyword(null,"dist-port","dist-port",605116572),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"cmd","cmd",-302931143),new cljs.core.Keyword(null,"dist-port","dist-port",605116572),new cljs.core.Keyword(null,"tid","tid",-901350880),f,new cljs.core.Keyword(null,"port","port",1534937262),p], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"port","port",1534937262)], null)], null));
});
tau.alpha.io.send_two_port = (function tau$alpha$io$send_two_port(tid1,tid2){
var c = (new MessageChannel());
tau.alpha.io.send_port.call(null,new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.state.ports),tid1)),tid2,c.port2);

return tau.alpha.io.send_port.call(null,new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(cljs.core.get.call(null,cljs.core.deref.call(null,tau.alpha.state.ports),tid2)),tid1,c.port1);
});
tau.alpha.io.dist_ports = (function tau$alpha$io$dist_ports(tid){
if(cljs.core.truth_(tau.alpha.state.on_screen_QMARK_.call(null))){
var ports = cljs.core.keys.call(null,cljs.core.dissoc.call(null,cljs.core.deref.call(null,tau.alpha.state.ports),tid));
return cljs.core.doall.call(null,cljs.core.map.call(null,((function (ports){
return (function (p1__9221_SHARP_){
return tau.alpha.io.send_two_port.call(null,tid,p1__9221_SHARP_);
});})(ports))
,ports));
} else {
return null;
}
});
tau.alpha.state.add_listeners.call(null,cljs.core.deref.call(null,tau.alpha.state.serve_handlers));

//# sourceMappingURL=io.js.map
