// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.example.core');
goog.require('cljs.core');
goog.require('tau.alpha.core');
goog.require('tau.alpha.example.automata');
goog.require('tau.alpha.example.utils');
goog.require('tau.alpha.example.fps');
goog.require('goog.object');
tau.alpha.core.set_conf_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"main","main",-2117802661),"tau.alpha.example.core",new cljs.core.Keyword(null,"log?","log?",-366002723),true], null));
tau.alpha.example.core.paint_automaton = (function tau$alpha$example$core$paint_automaton(ctx,width,height,iref){
var idata = ctx.createImageData(width,height);
idata.data.set(cljs.core.deref.call(null,iref));

return ctx.putImageData(idata,(0),(0),(0),(0),width,height);
});
if(cljs.core.truth_(tau.alpha.core.on_screen_QMARK_.call(null))){
tau.alpha.example.core.fps_div = document.getElementById("fps");

cljs.core.add_watch.call(null,tau.alpha.example.fps.fps,new cljs.core.Keyword(null,"fps","fps",683533296),(function (k,a,o,n){
return tau.alpha.example.core.fps_div.innerHTML = ["<p>",cljs.core.str.cljs$core$IFn$_invoke$arity$1(n),"</p>"].join('');
}));
} else {
}
tau.alpha.example.core.start = (function tau$alpha$example$core$start(on_QMARK_,state,ctx,width,height){
if(cljs.core.truth_(cljs.core.deref.call(null,on_QMARK_))){
return tau.alpha.example.utils.raf.call(null,(function (){
cljs.core.swap_BANG_.call(null,state,tau.alpha.example.automata.get_frame);

tau.alpha.example.core.paint_automaton.call(null,ctx,width,height,state);

return tau.alpha.example.core.start.call(null,on_QMARK_,state,ctx,width,height);
}));
} else {
return null;
}
});
tau.alpha.example.core.setup = (function tau$alpha$example$core$setup(ctx,width){
var state = cljs.core.atom.call(null,tau.alpha.example.automata.make_initial_conditions.call(null,width));
var on_QMARK_ = cljs.core.atom.call(null,false);
ctx.fillColor = "#ffffff";

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"state","state",-1988618099),state,new cljs.core.Keyword(null,"on?","on?",-74017086),on_QMARK_], null);
});
tau.alpha.example.core.hookup = (function tau$alpha$example$core$hookup(astr){
var map__9335 = tau.alpha.example.utils.actx.call(null,astr);
var map__9335__$1 = ((((!((map__9335 == null)))?(((((map__9335.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9335.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9335):map__9335);
var ctx = cljs.core.get.call(null,map__9335__$1,new cljs.core.Keyword(null,"ctx","ctx",-493610118));
var width = cljs.core.get.call(null,map__9335__$1,new cljs.core.Keyword(null,"width","width",-384071477));
var height = cljs.core.get.call(null,map__9335__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var canvas = cljs.core.get.call(null,map__9335__$1,new cljs.core.Keyword(null,"canvas","canvas",-1798817489));
var map__9336 = tau.alpha.example.core.setup.call(null,ctx,width);
var map__9336__$1 = ((((!((map__9336 == null)))?(((((map__9336.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9336.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9336):map__9336);
var state = cljs.core.get.call(null,map__9336__$1,new cljs.core.Keyword(null,"state","state",-1988618099));
var on_QMARK_ = cljs.core.get.call(null,map__9336__$1,new cljs.core.Keyword(null,"on?","on?",-74017086));
return tau.alpha.example.utils.on_click.call(null,canvas,((function (map__9335,map__9335__$1,ctx,width,height,canvas,map__9336,map__9336__$1,state,on_QMARK_){
return (function (){
cljs.core.swap_BANG_.call(null,on_QMARK_,cljs.core.not);

return tau.alpha.example.core.start.call(null,on_QMARK_,state,ctx,width,height);
});})(map__9335,map__9335__$1,ctx,width,height,canvas,map__9336,map__9336__$1,state,on_QMARK_))
);
});
if(cljs.core.truth_(tau.alpha.core.on_screen_QMARK_.call(null))){
tau.alpha.example.core.hookup.call(null,"a1");

tau.alpha.example.core.hookup.call(null,"b1");

tau.alpha.example.core.hookup.call(null,"c1");

tau.alpha.example.core.hookup.call(null,"d1");

tau.alpha.example.core.hookup.call(null,"e1");

tau.alpha.example.core.hookup.call(null,"f1");
} else {
}
tau.alpha.example.core.local_store = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
tau.alpha.example.core.process_results = (function tau$alpha$example$core$process_results(tau__$1){
return tau.alpha.example.utils.raf.call(null,(function (){
var map__9339 = cljs.core.deref.call(null,tau.alpha.example.core.local_store).call(null,tau__$1);
var map__9339__$1 = ((((!((map__9339 == null)))?(((((map__9339.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9339.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9339):map__9339);
var ctx = cljs.core.get.call(null,map__9339__$1,new cljs.core.Keyword(null,"ctx","ctx",-493610118));
var width = cljs.core.get.call(null,map__9339__$1,new cljs.core.Keyword(null,"width","width",-384071477));
var height = cljs.core.get.call(null,map__9339__$1,new cljs.core.Keyword(null,"height","height",1025178622));
var on_QMARK_ = cljs.core.get.call(null,map__9339__$1,new cljs.core.Keyword(null,"on?","on?",-74017086));
tau.alpha.example.core.paint_automaton.call(null,ctx,width,height,tau__$1);

return cljs.core.swap_BANG_.call(null,on_QMARK_,cljs.core.constantly.call(null,cljs.core.deref.call(null,on_QMARK_)));
}));
});
tau.alpha.example.core.swap_on_BANG_ = (function tau$alpha$example$core$swap_on_BANG_(tauon,tau__$1,work_fn){
return tau.alpha.io.do_on.call(null,tauon,(function (tau__$2,work_fn__$1){
cljs.core.swap_BANG_.call(null,tau__$2,work_fn__$1);

return tau.alpha.io.do_on.call(null,"screen",(function (tau__$3){
return tau.alpha.example.core.process_results.call(null,tau__$3);
}),null,tau__$2);
}),null,tau__$1,work_fn);
});
tau.alpha.example.core.run_loop_once = (function tau$alpha$example$core$run_loop_once(on_QMARK_,tau__$1){
if(cljs.core.truth_(cljs.core.deref.call(null,on_QMARK_))){
var map__9341 = cljs.core.deref.call(null,tau.alpha.example.core.local_store).call(null,tau__$1);
var map__9341__$1 = ((((!((map__9341 == null)))?(((((map__9341.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9341.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9341):map__9341);
var tauon = cljs.core.get.call(null,map__9341__$1,new cljs.core.Keyword(null,"tauon","tauon",1541936936));
return tau.alpha.example.core.swap_on_BANG_.call(null,tauon,tau__$1,tau.alpha.example.automata.get_frame);
} else {
return null;
}
});
tau.alpha.example.core.setup_tauon = (function tau$alpha$example$core$setup_tauon(p__9343){
var map__9344 = p__9343;
var map__9344__$1 = ((((!((map__9344 == null)))?(((((map__9344.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9344.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9344):map__9344);
var automaton = map__9344__$1;
var width = cljs.core.get.call(null,map__9344__$1,new cljs.core.Keyword(null,"width","width",-384071477));
var tauon = tau.alpha.core.tauon.call(null);
var tau__$1 = tau.alpha.core.tau.call(null,tau.alpha.example.automata.make_initial_conditions.call(null,width),new cljs.core.Keyword(null,"ab","ab",-839573926),true,new cljs.core.Keyword(null,"size","size",1098693007),((width * width) * (4)));
cljs.core.swap_BANG_.call(null,tau.alpha.example.core.local_store,cljs.core.assoc,tau__$1,cljs.core.merge.call(null,automaton,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"on?","on?",-74017086),cljs.core.atom.call(null,false),new cljs.core.Keyword(null,"tauon","tauon",1541936936),tauon,new cljs.core.Keyword(null,"tau","tau",89782904),tau__$1], null)));

return cljs.core.deref.call(null,tau.alpha.example.core.local_store).call(null,tau__$1);
});
tau.alpha.example.core.start_automaton = (function tau$alpha$example$core$start_automaton(on_QMARK_,tau__$1){
cljs.core.swap_BANG_.call(null,on_QMARK_,cljs.core.not);

return tau.alpha.example.core.run_loop_once.call(null,on_QMARK_,tau__$1);
});
tau.alpha.example.core.hookup_automaton = (function tau$alpha$example$core$hookup_automaton(astr){
var map__9346 = tau.alpha.example.utils.actx.call(null,astr);
var map__9346__$1 = ((((!((map__9346 == null)))?(((((map__9346.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9346.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9346):map__9346);
var automaton = map__9346__$1;
var canvas = cljs.core.get.call(null,map__9346__$1,new cljs.core.Keyword(null,"canvas","canvas",-1798817489));
var map__9347 = tau.alpha.example.core.setup_tauon.call(null,automaton);
var map__9347__$1 = ((((!((map__9347 == null)))?(((((map__9347.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__9347.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__9347):map__9347);
var tau__$1 = cljs.core.get.call(null,map__9347__$1,new cljs.core.Keyword(null,"tau","tau",89782904));
var on_QMARK_ = cljs.core.get.call(null,map__9347__$1,new cljs.core.Keyword(null,"on?","on?",-74017086));
tau.alpha.example.utils.on_click.call(null,canvas,((function (map__9346,map__9346__$1,automaton,canvas,map__9347,map__9347__$1,tau__$1,on_QMARK_){
return (function (){
return tau.alpha.example.core.start_automaton.call(null,on_QMARK_,tau__$1);
});})(map__9346,map__9346__$1,automaton,canvas,map__9347,map__9347__$1,tau__$1,on_QMARK_))
);

return cljs.core.add_watch.call(null,on_QMARK_,tau__$1,((function (map__9346,map__9346__$1,automaton,canvas,map__9347,map__9347__$1,tau__$1,on_QMARK_){
return (function (){
return tau.alpha.example.core.run_loop_once.call(null,on_QMARK_,tau__$1);
});})(map__9346,map__9346__$1,automaton,canvas,map__9347,map__9347__$1,tau__$1,on_QMARK_))
);
});
if(cljs.core.truth_(tau.alpha.core.on_screen_QMARK_.call(null))){
tau.alpha.example.core.hookup_automaton.call(null,"a2");

tau.alpha.example.core.hookup_automaton.call(null,"b2");

tau.alpha.example.core.hookup_automaton.call(null,"c2");

tau.alpha.example.core.hookup_automaton.call(null,"d2");

tau.alpha.example.core.hookup_automaton.call(null,"e2");

tau.alpha.example.core.hookup_automaton.call(null,"f2");
} else {
}
if(cljs.core.truth_(tau.alpha.core.on_screen_QMARK_.call(null))){
tau.alpha.example.core.square = document.getElementById("square-animation");

tau.alpha.example.core.top = cljs.core.atom.call(null,(0));

tau.alpha.example.core.move = (function tau$alpha$example$core$move(){
if(((300) >= cljs.core.deref.call(null,tau.alpha.example.core.top))){
cljs.core.swap_BANG_.call(null,tau.alpha.example.core.top,cljs.core.inc);
} else {
cljs.core.reset_BANG_.call(null,tau.alpha.example.core.top,(0));
}

return goog.object.set(tau.alpha.example.core.square.style,"top",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,tau.alpha.example.core.top)),"px"].join(''));
});

setInterval(tau.alpha.example.core.move,(10));
} else {
}

//# sourceMappingURL=core.js.map
