// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.example.automata');
goog.require('cljs.core');
tau.alpha.example.automata.make_initial_conditions = (function tau$alpha$example$automata$make_initial_conditions(width){
var w = ((4) * width);
var size = ((w * width) - (1));
var a = (new Array(size));
var b = (new Uint8ClampedArray((function (){var a__4402__auto__ = a;
var l__4403__auto__ = a__4402__auto__.length;
var _ = cljs.core.aclone.call(null,a__4402__auto__);
var i = (0);
while(true){
if((i < l__4403__auto__)){
(_[i] = ((cljs.core._EQ_.call(null,(0),cljs.core.mod.call(null,(i + (1)),(4))))?(255):(0)));

var G__7535 = (i + (1));
i = G__7535;
continue;
} else {
return _;
}
break;
}
})()));
b.set([(255),(255),(255),(255)],((width * (2)) | (0)));

return b;
});
tau.alpha.example.automata.white = (new Uint8ClampedArray(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(0),(0),(255)], null)));
tau.alpha.example.automata.black = (new Uint8ClampedArray(new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [(255),(255),(255),(255)], null)));
tau.alpha.example.automata.make_rule = (function tau$alpha$example$automata$make_rule(v1,v2,v3,v4,v5,v6,v7,v8){
return cljs.core.PersistentArrayMap.createAsIfByAssoc([new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(255),(255),(255)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v1),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(255),(255),(0)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v2),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(255),(0),(255)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v3),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(255),(0),(0)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v4),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(255),(255)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v5),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(255),(0)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v6),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(0),(255)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v7),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(0),(0),(0)], null),new cljs.core.PersistentArrayMap(null, 2, [(0),tau.alpha.example.automata.white,(1),tau.alpha.example.automata.black], null).call(null,v8)]);
});
tau.alpha.example.automata.wrap_sub = (function tau$alpha$example$automata$wrap_sub(the_state,the_index){
var size = the_state.length;
var norm = (the_index + ((4) * size));
return cljs.core.vec.call(null,cljs.core.map.call(null,((function (size,norm){
return (function (p1__7536_SHARP_){
return (the_state[cljs.core.rem.call(null,(norm + p1__7536_SHARP_),size)]);
});})(size,norm))
,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [(-3),(0),(4)], null)));
});
tau.alpha.example.automata.make_stepper = (function tau$alpha$example$automata$make_stepper(the_rule,width){
return (function (state){
var width_7537__$1 = (((4) * width) - (1));
var x_7538 = ((state.length - width_7537__$1) - (4));
var the_state_7539 = state;
var i_7540 = (0);
while(true){
if((i_7540 < x_7538)){
var pre_7541 = tau.alpha.example.automata.wrap_sub.call(null,the_state_7539,i_7540);
var res_7542 = the_rule.call(null,pre_7541);
the_state_7539.set(res_7542,((width_7537__$1 + (1)) + i_7540));

var G__7543 = the_state_7539;
var G__7544 = (i_7540 + (4));
the_state_7539 = G__7543;
i_7540 = G__7544;
continue;
} else {
}
break;
}

var r_7545 = (((4) * width) * width);
var l_7546 = (r_7545 - ((4) * width));
var sl_7547 = state.slice(l_7546,r_7545);
state.set(sl_7547,(0));

return (new Uint8ClampedArray(state));
});
});
tau.alpha.example.automata.rule30 = tau.alpha.example.automata.make_rule.call(null,(0),(0),(0),(1),(1),(1),(1),(0));
tau.alpha.example.automata.rule_30_stepper = tau.alpha.example.automata.make_stepper.call(null,tau.alpha.example.automata.rule30,(256));
tau.alpha.example.automata.get_frame = (function tau$alpha$example$automata$get_frame(a){
return tau.alpha.example.automata.rule_30_stepper.call(null,a);
});

//# sourceMappingURL=automata.js.map
