// Compiled by ClojureScript 1.10.339 {}
goog.provide('tau.alpha.example.utils');
goog.require('cljs.core');
tau.alpha.example.utils.raf = (((typeof requestAnimationFrame !== 'undefined'))?requestAnimationFrame:null);
tau.alpha.example.utils.on_click = (function tau$alpha$example$utils$on_click(el,afn){
return el.addEventListener("click",afn);
});
tau.alpha.example.utils.actx = (function tau$alpha$example$utils$actx(astr){
var acanvas = document.getElementById(astr);
var ctx = acanvas.getContext("2d");
var w = acanvas.width;
var h = acanvas.height;
var idata = ctx.getImageData((0),(0),w,h);
var data = new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"canvas","canvas",-1798817489),acanvas,new cljs.core.Keyword(null,"ctx","ctx",-493610118),ctx,new cljs.core.Keyword(null,"width","width",-384071477),w,new cljs.core.Keyword(null,"height","height",1025178622),h,new cljs.core.Keyword(null,"image-data","image-data",-377483758),idata], null);
return data;
});

//# sourceMappingURL=utils.js.map
